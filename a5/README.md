NOTE: This README.md file should be placed at the root of each of your repos directories.
>
>Also, this file must use Markdown syntax, and provide project documentation as per below--otherwise, points will be deducted.
>

# LIS 4381 Mobile Web Application Development

## Noah LaPace

### LIS 4381 Requirements:

Sub-Heading:

1. Create a Online Portfolio 
2. Complete skillset screenshots 13-15
3. Chapter Questions (Chapters 11, 12, 19)

#### README.md file should include the following items:

* Screenshots of Online Portfolio
* Screenshot of skillset 13 - Sphere Volume Calculator
* Screenshot of skillset 14 - Simple Calculator
* Screenshot of skillset 15 - Write/Read File

This is a blockquote.

This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

| index.php             | add_petstore.php (invalid)
| -----------                                   | -----------
| ![Passed Validation](img/indexphp.png)| ![Add petstore (invalid)](img/addpetinv.png)

Screenshot of add_petstore_process.php (Failed Validation) :

![Online Portfolio](img/addpetstorefail.png "Online Portfolio")

Screenshot of add_petstore.php (valid):

![Array List Screenshot](img/addpetvalid.png "Array List")

Screenshot of add_petstore_process.php (Failed Validation):

![Alpha Numeric Special Screenshot](img/addpetstorevalidation.png "Alpha Numeric Special")

Screenshot of skillset 13:

![Temperature Conversion Screenshot](img/skillset13.png "Temperature Conversion")

| Simple Calculator (index.php)            | Simple Calculator (process_functions.php)
| -----------                                   | -----------
| ![Passed Validation](img/skillset14add.png)| ![Add petstore (invalid)](img/skillset14addsolve.png)

| Simple Calculator (index.php)             | Simple Calculator (process_functions.php)
| -----------                                   | -----------
| ![Passed Validation](img/division14.png)| ![Add petstore (invalid)](img/cannotzero.png)

| Write/Read File (index.php)            | Write/Read File (process.php)
| -----------                                   | -----------
| ![Passed Validation](img/writeread.png)| ![Add petstore (invalid)](img/writeread2.png)

