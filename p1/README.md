> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Noah LaPace

### Project 1 Requirements: 

*Sub-Heading:*

1. Skillsets 7-9 Completed Programs
2. Android Studio - My Buisness Card!
3. Chapter Questions (Chs 7,8)

#### README.md file should include the following items:

* Screenshot of skillsets 7-9
* Screenshot of the applications first interface
* Screenshot of the applications second interface

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Android Studio - My Buisness Card! opening user interface*:

![My Buisness Card! opening user interface](img/interface1.png "My Buisness Card! opening user interface")

*Screenshot of Android Studio - My Buisness Card! second user interface*:

![My Buisness Card! second user interface](img/interface2.png "My Buisness Card! second user interface")

*Screenshot of Skillset 7 Random Number Generator Data Validation*:

![Skillset 7 Random Number Generator Data Validation](img/skillset7.png "Skillset Random Number Generator Data Validation")

*Screenshot of Skillset 8 Largest Three Numbers*:

![Skillset 8 Largest Three Numbers](img/skillset8.png "Skillset 8 Largest Three Numbers")

*Screenshot of Skillset 9 Array Runtime Data Validation*:

![Skillset 9 Array Runtime Data Validation](img/skillset9.png "Skillset 9 Array Runtime Data Validation")


