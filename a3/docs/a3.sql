-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema nal20z
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `nal20z` ;

-- -----------------------------------------------------
-- Schema nal20z
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `nal20z` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `nal20z` ;

-- -----------------------------------------------------
-- Table `nal20z`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nal20z`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `nal20z`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` CHAR(9) NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `nal20z`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nal20z`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `nal20z`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` CHAR(9) NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `nal20z`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nal20z`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `nal20z`.`pet` (
  `pet_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `nal20z`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `nal20z`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `nal20z`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `nal20z`;
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Dogs', '123 Main St.', 'Tallahassee ', 'FL', '111137790', 8500001111, 'dogs@gmail.com', 'http://www.dogs.com', 99000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Cats', '234 Main St.', 'Tallahassee ', 'FL', '111139790', 8501112222, 'cats@gmail.com', 'http://www.cats.com', 90000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Fish', '345 Main St.', 'Tallahassee ', 'FL', '111138790', 8502223333, 'fish@gmail.com', 'http://www.fish.com', 80000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Snakes', '456 Main St.', 'Tallahassee ', 'FL', '111130790', 8503334444, 'snakes@gmail.com', 'http://www.snakes.com', 70000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Frogs', '567 Main St.', 'Tallahassee ', 'FL', '111131790', 8504445555, 'frogs@gmail.com', 'http://www.frogs.com', 60000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Lizards', '678 Main St.', 'Tallahassee ', 'FL', '111132790', 8505556666, 'lizards@gmail.com', 'http://www.lizards.com', 50000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Birds', '789 Main St.', 'Tallahassee ', 'FL', '111133790', 8506667777, 'birds@gmail.com', 'http://www.birds.com', 40000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Hamsters', '111 Main St.', 'Tallahassee ', 'FL', '111134790', 8507778888, 'hamsters@gmail.com', 'http://www.hamsters.com', 30000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Turtles', '222 Main St.', 'Tallahassee ', 'FL', '111135790', 8508889999, 'tutrles@gmail.com', 'http://www.turtles.com', 20000, NULL);
INSERT INTO `nal20z`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Got Rabbits', '333 Main St.', 'Tallahassee ', 'FL', '111136790', 8509990000, 'Rabbits@gmail.com', 'http://www.rabbits.com', 10000, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `nal20z`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `nal20z`;
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sponge', 'Bob', 'Pineapple St.', 'Bikini Bottom', 'PO', '111234568', 5611113333, 'bob@gmail.com', 900, 10000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Pat', 'Rick', 'Rock St.', 'Bikini Bottom', 'PO', '111234568', 5611114444, 'rick@gmail.com', 100, 9000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Squid', 'Ward', 'Stone St.', 'Bikini Bottom', 'PO', '111234568', 5611115555, 'ward@gmail.com', 800, 8000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Plank', 'Ton', 'Bucket St.', 'Bikini Bottom', 'PO', '111234566', 5611116666, 'ton@gmail.com', 700, 7000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Mr', 'Krabs', 'Krusty St.', 'Bikini Bottom', 'PO', '111234565', 5611117777, 'krabs@gmail.com', 600, 6000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'San', 'Dy', 'Dome St.', 'Bikini Bottom', 'PO', '111234564', 5611118888, 'sandy@gmail.com', 500, 5000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Gar', 'Ry', 'Pineapple St.', 'Bikini Bottom', 'PO', '111234563', 5611119999, 'garry@gmail.com', 400, 4000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Lar', 'Ry', 'Anchor St.', 'Bikini Bottom', 'PO', '111234562', 5611110000, 'larry@gmail.com', 300, 3000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Nep', 'Tune', 'Castle St.', 'Bikini Bottom', 'PO', '111234561', 5611111111, 'tune@gmail.com', 999, 2000, NULL);
INSERT INTO `nal20z`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Dutch', 'Man', 'Ship St.', 'Bikini Bottom', 'PO', '111234567', 5611112222, 'man@gmail.com', 200, 1000, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `nal20z`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `nal20z`;
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'Cane Corso', 'm', 500, 900, 20, 'black', '2009-07-01', 'y', 'n', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'Sphinx', 'f', 400, 800, 19, 'black', '2009-07-09', 'n', 'y', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'Lionfish', 'm', 300, 700, 18, 'orange', '2009-07-08', 'y', 'y', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 4, 'Parrot', 'f', 200, 600, 17, 'brown', '2009-07-07', 'n', 'y', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 5, 'Gecko', 'm', 100, 500, 16, 'white', '2009-07-06', 'y', 'n', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 5, 'Orange Spotter', 'f', 450, 500, 15, 'green', '2009-07-05', 'n', 'n', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 4, 'Stripped Zebra', 'm', 350, 400, 14, 'yellow', '2009-07-04', 'y', 'n', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'Spotted Leopard', 'f', 250, 300, 13, 'blue', '2009-07-03', 'n', 'y', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'Snapping Tutrle', 'm', 150, 200, 12, 'orange', '2009-07-02', 'y', 'y', NULL);
INSERT INTO `nal20z`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'Curly Tail Rabbit', 'f', 125, 150, 11, 'white', '2009-07-01', 'n', 'y', NULL);

COMMIT;

