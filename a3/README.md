> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Noah LaPace

### Assignment 3 Requirements: 

*Sub-Heading:*

1. Skillsets 4-6 Completed Programs
2. Android Studio - My Event Application
3. Chapter Questions (Chs 5,6)

#### README.md file should include the following items:

* Screenshot of skillsets 4-6
* Screenshot of Pets R-US ERD
* Screenshot of each table from the ERD
* Links to a3 files

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Android Studio - Concert App opening user interface:*:

![Android Studio - Concert App opening user interface](img/AndroidEmulator.png "Android Studio - Concert App opening user interface")

*Screenshot of Android Studio - Concert App processing user input*:

![Android Studio - Concert App processing user input](img/CalculatedApp.png "Android Studio - Concert App processing user input")

*Screenshot of MySQL ERD*:

![MySQL ERD](img/ERD.jpg "MySQL ERD")

*Screenshot of Petstore table*:

![Petstore table](img/PetStore.jpg "Petstore table")

*Screenshot of Pet Table*:

![Pet Table](img/Pet.jpg "Pet Table")

*Screenshot of Customer Table*:

![Customer Table ](img/customer.jpg "Customer Table")

*Screenshot of Skillset 4 Decision Structures*:

![Skillset 4 Decision Structures](img/Skillset4.png "Skillset 4 Decision Strucutres")

*Screenshot of Skillset 5 Random Number Generator*:

![Skillset 5 Random Number Generator](img/Skillset5.png "Skillset 5 Random Number Generator")

*Screenshot of Skillset 6 Methods*:

![Skillset 6 Methods](img/Skillset6.png "Skillset 6 Methods")

*a3.mwb File*:
[MYSQL -a3.mwb](https://bitbucket.org/nal20z/lis4381/src/master/a3/docs/A3.mwb "A3.mwb File")

*a3.sql File*:
[MySQL - a3.sql](https://bitbucket.org/nal20z/lis4381/src/master/a3/docs/a3.sql "A3.sql File")
