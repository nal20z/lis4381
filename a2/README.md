> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Noah LaPace

### Assignment 2 Requirements: 

*Sub-Heading:*

1. Created a Recipe app using Android Studio
2. Completed skillset screenshots 1-3
3. Chapter Questions (Ch 3, 4)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A2;
* Screenshot of skillset 1 - Even or Odd
* Screenshot of skillset 2 - Largest Number
* Screenshot of skillset 3 - Arrays and Loops
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Bruschetta Recipe Screen 1*:

![Bruschetta Recipe Screen 1](img/screen1.png "Bruschetta Recipe Screen 1")

*Screenshot of Bruschetta Recipe Screen 2*:

![Bruschetta Recipe Screen 2](img/Screen2.png "Bruschetta Recipe Screen 2")

*Screenshot of Skillset 1 Even or Odd*:

![Skillset 1 Even or Odd Program](img/skillset1.png "Skillset 1 Even or Odd Program")

*Screenshot of Skillset 2 largest Number*:

![Skillset 2 Largest Number Program](img/Skillset2.png "Skillset 2 Largest Number Program")

*Screenshot of Skillset 3 Arrays and Loops*:

![Skillset 3 Arrays and Loops Program](img/Skillset3.png "Skillset 3 Arrays and Loops Program")

