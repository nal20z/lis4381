NOTE: This README.md file should be placed at the root of each of your repos directories.
>
>Also, this file must use Markdown syntax, and provide project documentation as per below--otherwise, points will be deducted.
>

# LIS 4381 Mobile Web Application Development

## Noah LaPace

### LIS 4381 Requirements:

Sub-Heading:

1. Create a Online Portfolio 
2. Complete Project 2
3. Chapter Questions (Chapters 11, 12, 19)

#### README.md file should include the following items:

* Screenshots of Online Portfolio
* Create project 2
* Screenshot of skillset 14 - Simple Calculator
* Screenshot of skillset 15 - Write/Read File

This is a blockquote.

This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

| Homescreen            | index.php
| -----------                                   | -----------
| ![Passed Validation](img/homescreen.png)| ![Add petstore (invalid)](img/index.png)

Screenshot of edit.php :

![Online Portfolio](img/edit.png "Online Portfolio")

Screenshot of failed (valid):

![Array List Screenshot](img/update.png "Array List")

Screenshot of add_petstore_process.php (Failed Validation):

![Alpha Numeric Special Screenshot](img/passed.png "Alpha Numeric Special")
Screenshot of delete:

![Temperature Conversion Screenshot](img/succdelete.png "Temperature Conversion")

Screenshot of RSS:

![Temperature Conversion Screenshot](img/rss.png "Temperature Conversion")
