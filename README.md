> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 Mobile Web Appilcation Noah LaPace

## Noah LaPace

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and my team quotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Complete assigned skillset numbers 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Complete skillset 4-6
    - Three tables of ten database records
    - Create moblie web application called "My Event"
    - Create and Connect database for mobile web application 

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Complete skillset 7-9
    - Created Borders on image
    - Create moblie web application called "My Buisness Card!"

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Complete skillset 10-12
    - Test Validations with JQuery
    - Using Bootstrap

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete skillset 13-15
    - Test Validations JQuery
    - Create a5 using Bootstrap w/ PHP

6. [A5 README.md](p2/README.md "My A5 README.md file")
    - Complete skillset 13-15
    - Test Validations JQuery
    - Create a5 using Bootstrap w/ PHP