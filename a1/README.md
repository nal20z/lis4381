> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Noah LaPace

### Assignment 1 Requirements: 

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init creates a new git repository
2. git status show the state of the working directory and staging area
3. git add allows you to add changes in a working directory to the staging area.
4. git commit captures a snapshot of the project's current staged changes.
5. git push is used to upload local repository content to a remote repository
6. git pull is a command that is used to update the loacl version of a repository
7. git clone is used to create an existing repository and make a clone or copy.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nal20z/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")