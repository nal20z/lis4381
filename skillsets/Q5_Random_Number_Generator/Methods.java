import java.util.*;
import java.util.Scanner;
import java.lang.*;

public class Methods 
{
    
    public static void getRequirements()
    {
        System.out.println("Developer: Noah A. LaPace");
        System.out.println("This generates numbers in a specific range that the user provided");
        System.out.println("Implement random number generator in while. do...while, for, enhanced...for loops.");

    }

    public static void doRan()
    {
        Scanner sc = new Scanner(System.in);
        int range1;
        int range2;
        int numbersNeed;
        
        System.out.print("Integer.Min_Value: ");
        range1 = sc.nextInt();

        System.out.print("Interger.Max_Value: ");
        range2 = sc.nextInt();
        
        System.out.print("Enter how many numbers you need: ");
        numbersNeed = sc.nextInt();

        int times = 0;
        System.out.println("While loop");
        while (times < numbersNeed)
        {
            System.out.println(randInt(range1, range2));
            times++;
        }

        System.out.println();
        System.out.println("for loop");
        for (int t = 0; t < numbersNeed; t++)
        {
            System.out.println(randInt(range1, range2));

        }
        
        System.out.println();
        System.out.println("enhanced for loop");

        int[] intArray = new int[numbersNeed];
        for (int m = 0; m < numbersNeed; m++)
        {
            intArray[m] = randInt(range1, range2);
        
        }
        for (int f : intArray)
        {
            System.out.println(f);
        }

        System.out.println();
        System.out.println("do while loop");
        int b = 0;

            do
            {
                System.out.println(randInt(range1, range2));
                b++;

            }while (b < numbersNeed);



        }

        public static int randInt(int range1, int range2)
        {
            Random ran = new Random();

            int fin = ran.nextInt(range1, range2);
            return fin;
        }


    }


