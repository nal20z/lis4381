import java.util.*; 
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Noah LaPace");

        System.out.println("Use following values: dog, cat, bird, fish, insect");

        System.out.println("Use the following loop structures: for, enhanced for, while, do..while");

        System.out.println();

        System.out.println("Note: pretest loops: for, enhanced for, while. posttest loops: do...while");

    }

    public static void arrayAndLoops() {
        // initialize string array variable animals
        String[] animals = { "dog", "cat", "bird", "fish", "insect" };

        // for loop to access array elements

        System.out.println("For loop");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i]);
        }

        System.out.println();

        // enhanced for loop to access array elements

        System.out.println("Enhanced for loop");
        for (String f : animals) {
            System.out.println(f);
        }

        System.out.println();

        System.out.println("while loop");

        int o = 0;

        while (o < animals.length) {
            System.out.println(animals[o]);
            o++;
        }

        System.out.println();

        // do while loop
        System.out.println("do while loop");

        int j = 0;
        do {
            System.out.println(animals[j]);
            j++;
        } while (j < animals.length);
    }// end of arrayAndLoops function

}
// end of the class
